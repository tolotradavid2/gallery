class Modal {

    /**
     * Take the element
     * @param {HTMLDivElement} element
     */
    constructor(element) {
        this.imgContainer = element
        this.items = this.imgContainer.children
        this.currentItem = null
        this.imgListenClick()
        this.createModal()
        this.closeModal()
    }

    createModal() {
        this.body = document.querySelector("body")
        this.modalBackground = this.createDivWithClass('modal__background')
        this.modalContainer = this.createDivWithClass('modal__container')
        this.modalBackground.appendChild(this.modalContainer)
        this.body.appendChild(this.modalBackground)
        this.createNavigation()
    }

    /**
     *
     * @param {String} className
     * @returns {HTMLDivElement}
     */
    createDivWithClass(className) {
        let div = document.createElement("div")
        div.setAttribute("class", className)
        return div
    }

    /**
     *
     * @param {String} text
     * @param {String} className
     * @returns {HTMLButtonElement}
     */
    createButtonWithClass(text, className) {
        let button = document.createElement("button")
        button.setAttribute("class", className)
        button.innerHTML = text
        return button
    }

    /**
     * Listen to the users click
     */
    imgListenClick() {
        for (let i = 0; i < this.items.length; i++) {
            this.items[i].addEventListener("click", () => {
                this.showModal(this.items[i])
                this.currentItem = i
            })
        }
    }

    /**
     *
     * @param {} imgItem
     */
    showModal(imgItem) {
        this.modalBackground.classList.add("modal__background--show")
        this.modalContainer.classList.add("modal__container--show")
        this.imgContainer.classList.add("blur")
        this.modalContainer.innerHTML = imgItem.innerHTML
    }

    closeModal() {
        const close = this.createButtonWithClass('&times', 'close')
        this.modalBackground.appendChild(close)
        close.addEventListener("click", (e) => {
            if (e.target.className === "modal__background modal__background--show") {
                
            }
            this.modalBackground.classList.remove("modal__background--show")
                this.modalContainer.classList.remove("modal__container--show")
                this.imgContainer.classList.remove("blur")
                this.modalContainer.innerHTML = ''
        })
    }

    showCarousel() {
        this.carousel = this.createDivWithClass('carousel')
        this.body.appendChild(this.carousel)
    }

    createNavigation() {
        let prev = this.createButtonWithClass('<', 'modal__prev')
        let next = this.createButtonWithClass('>', 'modal__next')
        prev.addEventListener('click', this.prev.bind(this))
        next.addEventListener('click', this.next.bind(this))
        this.modalBackground.appendChild(prev)
        this.modalBackground.appendChild(next)
    }

    next() {
        this.currentItem++
        if (this.currentItem >= this.items.length) {
            this.currentItem = 0
        }
        this.showModal(this.items[this.currentItem])
    }

    prev() {
        if (this.currentItem === 0) {
            this.currentItem = this.items.length
        }
        this.currentItem--
        this.showModal(this.items[this.currentItem])
    }
}


new Modal(document.querySelector(".img__container"))
